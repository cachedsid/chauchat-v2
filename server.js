const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const socket = require('socket.io');
const fs = require('fs');
const publicDir = path.join(__dirname, 'public');

app.use(cookieParser());

app.get('/chat.html', (req, res) => {
  if (!(req.cookies.username && req.cookies.password)) {
    res.redirect('/log.html');
    return;
  }

  const userData = JSON.parse(fs.readFileSync('users.json', 'utf8'));
  const getPassword = (() => {
    let keyValuePairs = {};
    userData.forEach(user => {
      keyValuePairs[user.username] = user.password;
    });
    return keyValuePairs;
  })();

  if (getPassword[req.cookies.username] !== req.cookies.password) {
    res.redirect('log.html?login=failed');
    return;
  }

  res.sendFile(path.join(publicDir, 'chat.html'));
});
app.post('/rauth', bodyParser.urlencoded({ extended: false }), (req, res) => {
  if (!(req.body.username && req.body.password)) {
    res.status(400).redirect('/log.html');
    return;
  }

  const userData = JSON.parse(fs.readFileSync('users.json', 'utf8'));
  const usernames = userData
    .map(user => user.username);

  if (usernames.includes(req.body.username.slice(0, 12).trim())) {
    res.status(401).redirect('log.html?unique=false');
    return;
  }

  let newUser = {
    username: req.body.username.toString().slice(0, 12).trim(),
    password: req.body.password.toString().slice(0, 25)
  };

  userData.push(newUser);

  fs.writeFileSync('users.json', JSON.stringify(userData, null, '\t'), 'utf8');

  res.cookie('username', newUser.username);
  res.cookie('password', newUser.password);

  res.redirect('/chat.html');
});
app.post('/lauth', bodyParser.urlencoded({ extended: false }), (req, res) => {
  res.cookie('username', req.body.username);
  res.cookie('password', req.body.password);

  res.redirect('/chat.html');
});

app.use(express.static('public'));

app.use('/scripts', express.static('public/scripts'));
app.use('/styles', express.static('public/styles'));

const server = app.listen(process.env.OPENSHIFT_NODEJS_PORT || 80, process.env.OPENSHIFT_NODEJS_IP || '24.13.74.38', () => {
  console.log(`Server is listening on port ${ process.env.PORT || 80 }`);
});

let usersOnline = 0;
const chatArchive = [];
const io = socket(server);

const chatNsp = io.of('/chat.html');

chatNsp.on('connect', socket => {
  socket.on('chat', chat => {
    const userData = JSON.parse(fs.readFileSync('users.json', 'utf8'));

    const getPassword = (() => {
      let keyValuePairs = {};
      userData.forEach(user => {
        keyValuePairs[user.username] = user.password;
      });
      return keyValuePairs;
    })();

    if (getPassword[chat.username] === chat.password) {
      chatArchive.push([chat.username, chat.chat]);
    }

    chatNsp.emit('chat', chatArchive);
  });
  socket.on('get-users-online', () => {
    socket.emit('get-users-online', usersOnline);
  });
  socket.on('join', data => {
    usersOnline++;
    socket.emit('chat', chatArchive);
  });
  socket.on('disconnect', () => {
    usersOnline--;
  });
});
