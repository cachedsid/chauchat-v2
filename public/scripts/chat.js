const socket = io('http://chauchat.ddns.net/chat.html');

const cookie = cookieToObj(document.cookie);

$(() => {
  let chatwindow = $('.chatwindow');
  let chatbox = $('.chat');
  let chat = $('input[name=chat]');

  socket.emit('join', {
    username: cookie.username,
    chat: chat.val(),
    password: cookie.password
  });

  chatbox.submit(function(e) {
    e.preventDefault();

    socket.emit('chat', {
      username: cookie.username,
      chat: chat.val(),
      password: cookie.password
    });

    chat.val('');
  });

  socket.on('chat', chats => {
    chatwindow.html('');

    chats.forEach(data => {
      let line = $('<p></p>');
      let user = $('<b></b>').text(`<${ data[0] }>: `);
      let content = $('<span></span>').text(data[1]);

      line.append(user).append(content);

      chatwindow.append(line);
    });
  });

  socket.emit('get-users-online');
  setInterval(() => {
    socket.emit('get-users-online');
  }, 500);
  socket.on('get-users-online', users => {
    $('#users-online > span').text(users);
  });
});

function cookieToObj(cookie) {
  let parsedCookie = {};

  cookie
    .split(/;\s*/)
    .forEach(pair => {
      let parsedPair = pair.split('=');

      parsedCookie[parsedPair[0]] = parsedPair[1];
    });

  return parsedCookie;
}
