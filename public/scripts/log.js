$(() => {
  if ($(document).getUrlParam('unique') === 'false') {
    $('#rerror-box').text(Error('User already exists'));
  } else if ($(document).getUrlParam('login') === 'failed') {
    $('#lerror-box').text(Error('Login failed'));
  }
});
